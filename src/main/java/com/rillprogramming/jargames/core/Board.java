package com.rillprogramming.jargames.core;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Board extends JPanel implements ActionListener {

    private Game game;

    private int B_WIDTH, B_HEIGHT;
    private int DELAY;

    public boolean inGame = true;

    private Timer timer;

    public Board(Game game) {
        this.B_WIDTH = game.getWidth();
        this.B_HEIGHT = game.getHeight();
        this.game = game;
        this.DELAY = game.getDelay();
        initBoard();
    }

    private void initBoard() {
        addKeyListener(new TAdapter(this, this.game.getKeyBinds()));
        setBackground(Color.black);
        setFocusable(true);

        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
    }

    public void initGame() {
        timer = new Timer(DELAY, this);
        timer.start();
    }

    public void actionPerformed(ActionEvent e) {
        if (inGame) {
            this.game.run();
        }
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.game.draw(g);
    }

    public Timer getTimer() {
        return this.timer;
    }
}
