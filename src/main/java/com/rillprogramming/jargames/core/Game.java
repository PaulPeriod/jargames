package com.rillprogramming.jargames.core;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Game extends JFrame {

    public int width, height, delay, point_size, all_dots;

    private List<KeyBind> keyBinds;
    private String title;
    private Board board;

    public Game(int width, int height, int point_size, String title, int delay) {
        this.width = width;
        this.height = height;
        this.point_size = point_size;
        this.keyBinds = new ArrayList<>();
        this.title = title;
        this.delay = delay;
        this.all_dots = (width / point_size) * (height / point_size);
        initUI();
    }

    private void initUI() {
        this.board = new Board(this);
        add(board);

        setResizable(false);
        pack();

        setTitle(this.title);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initGame();
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void addKeybind(KeyBind keyBind) {
        this.keyBinds.add(keyBind);
    }

    public List<KeyBind> getKeyBinds() {
        return this.keyBinds;
    }

    public int getDelay() {
        return this.delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public Board getBoard() {
        return this.board;
    }

    public abstract void initGame();
    public abstract void run();
    public abstract void draw(Graphics g);
}
