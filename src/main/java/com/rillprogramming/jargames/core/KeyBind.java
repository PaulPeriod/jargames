package com.rillprogramming.jargames.core;

public abstract class KeyBind {
    public abstract int getKey();
    public abstract void action();
}
