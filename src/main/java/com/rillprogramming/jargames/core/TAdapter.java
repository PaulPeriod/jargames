package com.rillprogramming.jargames.core;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

public class TAdapter extends KeyAdapter {

    private Board board;
    private List<KeyBind> keyBinds;

    public TAdapter(Board board, List<KeyBind> keyBinds) {
        this.board = board;
        this.keyBinds = keyBinds;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        for(KeyBind bind : keyBinds) {
            if(bind.getKey() == key) {
                bind.action();
            }
        }
    }
}
