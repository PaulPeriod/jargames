package com.rillprogramming.jargames.snake;

import com.rillprogramming.jargames.core.KeyBind;

import java.awt.event.KeyEvent;

public class LeftBind extends KeyBind {

    private Snake snake;

    public LeftBind(Snake snake) {
        this.snake = snake;
    }

    @Override
    public int getKey() {
        return KeyEvent.VK_LEFT;
    }

    @Override
    public void action() {
        if (!this.snake.directions[0][1]) {
            this.snake.directions[0][0] = true;
            this.snake.directions[1][0] = false;
            this.snake.directions[1][1] = false;
        }
    }
}
