package com.rillprogramming.jargames.snake;

import com.rillprogramming.jargames.core.KeyBind;

import java.awt.event.KeyEvent;

public class RightBind extends KeyBind {

    private Snake snake;

    public RightBind(Snake snake) {
        this.snake = snake;
    }

    @Override
    public int getKey() {
        return KeyEvent.VK_RIGHT;
    }

    @Override
    public void action() {
        if (!this.snake.directions[0][0]) {
            this.snake.directions[0][1] = true;
            this.snake.directions[1][0] = false;
            this.snake.directions[1][1] = false;
        }
    }
}
