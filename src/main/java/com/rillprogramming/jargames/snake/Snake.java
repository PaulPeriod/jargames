package com.rillprogramming.jargames.snake;

import com.rillprogramming.jargames.core.Game;

import javax.swing.*;
import java.awt.*;

public class Snake extends Game {

    private int RAND_POS;
    public int[] x;
    public int[] y;

    // [x-axes / y-axes][left-right / up-down]
    public boolean[][] directions = new boolean[2][2];

    private int length;
    // 0: x, 1: y
    private int[] apple;

    public Snake() {
        super(300,300, 10, "Snake", 140);
        directions[0][1] = true;
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame ex = new Snake();
            ex.setVisible(true);
        });
    }

    public void initGame() {
        apple = new int[2];
        x = new int[all_dots];
        y = new int[all_dots];
        length = 3;
        RAND_POS = 29;

        for (int z = 0; z < length; z++) {
            x[z] = 50 - z * 10;
            y[z] = 50;
        }

        registerKeybinds();
        locateApple();
        this.getBoard().initGame();
    }

    private void registerKeybinds() {
        this.addKeybind(new LeftBind(this));
        this.addKeybind(new RightBind(this));
        this.addKeybind(new UpBind(this));
        this.addKeybind(new DownBind(this));
    }

    @Override
    public void run() {
        checkApple();
        checkCollision();
        move();
    }

    private void checkApple() {
        if ((x[0] == apple[0]) && (y[0] == apple[1])) {
            length++;
            locateApple();
        }
    }

    private void locateApple() {
        int r = (int) (Math.random() * RAND_POS);
        apple[0] = ((r * point_size));

        r = (int) (Math.random() * RAND_POS);
        apple[1] = ((r * point_size));
    }

    private void checkCollision() {
        for (int z = length; z > 0; z--) {
            if ((x[0] == x[z]) && (y[0] == y[z])) {
                this.getBoard().inGame = false;
            }
        }

        if (y[0] >= height) {
            this.getBoard().inGame = false;
        }

        if (y[0] < 0) {
            this.getBoard().inGame = false;
        }

        if (x[0] >= width) {
            this.getBoard().inGame = false;
        }

        if (x[0] < 0) {
            this.getBoard().inGame = false;
        }

        if (!this.getBoard().inGame) {
            this.getBoard().getTimer().stop();
        }
    }

    public void move() {
        for (int z = length; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        // left
        if (directions[0][0]) {
            x[0] -= point_size;
        }

        // right
        if (directions[0][1]) {
            x[0] += point_size;
        }

        // up
        if (directions[1][0]) {
            y[0] -= point_size;
        }

        // down
        if (directions[1][1]) {
            y[0] += point_size;
        }
    }

    public void draw(Graphics g) {
        if (this.getBoard().inGame) {
            g.setColor(Color.GREEN);
            g.fillRect(apple[0], apple[1], point_size, point_size);

            for(int z = 0; z < length; z++) {
                if (z == 0) {
                    g.setColor(Color.RED);
                } else {
                    g.setColor(Color.WHITE);
                }
                g.fillRect(x[z], y[z], point_size, point_size);
            }

        } else {
            gameOver(g);
        }
    }

    private void gameOver(Graphics g) {
        String msg = "Game Over";
        Font small = new Font("Helvata", Font.BOLD, 14);
        FontMetrics metr = getFontMetrics(small);

        g.setColor(Color.WHITE);
        g.setFont(small);
        g.drawString(msg, (width - metr.stringWidth(msg)) / 2, height / 2);
    }
}
